!===================================================================
! these are diagonalization subroutines
! ====================================================
      SUBROUTINE DIAG(NMAX,N,A,EIG,DUMMY,TRANS,IERR)
      IMPLICIT INTEGER(I-N)
      IMPLICIT REAL*8(A-H,O-Z)
!
      DIMENSION A(NMAX,N),EIG(NMAX),DUMMY(NMAX),TRANS(NMAX,N)
!
      CALL TRED2(NMAX,N,A,EIG,DUMMY,TRANS)
      CALL TQL2(NMAX,N,EIG,DUMMY,TRANS,IERR)      
!
      RETURN
      END

! =========================================================
!*    SUBROUTINE TRED2( NM, N, A, D, E, Z )
!**   <TRED2> -- EISPACK ROUTINE -- SEE THEIR WRITEUP
      SUBROUTINE TRED2(NM,N,A,D,E,Z)
        IMPLICIT        DOUBLE PRECISION            (A-H,O-Z)
        IMPLICIT        INTEGER (I-N)
        DOUBLE PRECISION   A(NM,N),D(N),E(N),Z(NM,N)
        DATA     ZERO,ONE / 0.0D0, 1.0D0 /
      DO 100 I = 1, N
         DO 100 J = 1, I
            Z(I,J) = A(I,J)
  100 CONTINUE
      IF (N .EQ. 1) GO TO 320
!                FOR I=N STEP -1 UNTIL 2 DO --
      DO 300 II = 2, N
         I = N + 2 - II
         L = I - 1
         H = ZERO
         SCALE = ZERO
         IF (L .LT. 2) GO TO 130
!                SCALE ROW (ALGOL TOL THEN NOT NEEDED)
         DO 120 K = 1, L
  120    SCALE = SCALE + ABS(Z(I,K))
         IF (SCALE .NE. ZERO) GO TO 140
  130    E(I) = Z(I,L)
         GO TO 290
  140    DO 150 K = 1, L
            Z(I,K) = Z(I,K) / SCALE
            H = H + Z(I,K) * Z(I,K)
  150    CONTINUE
         F = Z(I,L)
         G = - SIGN( SQRT(H),F)
         E(I) = SCALE * G
         H = H - F * G
         Z(I,L) = F - G
         F = ZERO
         DO 240 J = 1, L
            Z(J,I) = Z(I,J) / (SCALE * H)
            G = ZERO
!                FORM ELEMENT OF A*U
            DO 180 K = 1, J
  180       G = G + Z(J,K) * Z(I,K)
            JP1 = J + 1
            IF (L .LT. JP1) GO TO 220
            DO 200 K = JP1, L
  200       G = G + Z(K,J) * Z(I,K)
!                FORM ELEMENT OF P
  220       E(J) = G / H
            F = F + E(J) * Z(I,J)
  240    CONTINUE
         HH = F / (H + H)
!                FORM REDUCED A
         DO 260 J = 1, L
            F = Z(I,J)
            G = E(J) - HH * F
            E(J) = G
            DO 260 K = 1, J
               Z(J,K) = Z(J,K) - F * E(K) - G * Z(I,K)
  260    CONTINUE
         DO 280 K = 1, L
  280    Z(I,K) = SCALE * Z(I,K)
  290    D(I) = H
  300 CONTINUE
  320 D(1) = ZERO
      E(1) = ZERO
!                ACCUMULATION OF TRANSFORMATION MATRICES
      DO 500 I = 1, N
         L = I - 1
         IF (D(I) .EQ. ZERO) GO TO 380
         DO 360 J = 1, L
            G = ZERO
            DO 340 K = 1, L
  340       G = G + Z(I,K) * Z(K,J)
            DO 360 K = 1, L
               Z(K,J) = Z(K,J) - G * Z(K,I)
  360    CONTINUE
  380    D(I) = Z(I,I)
         Z(I,I) = ONE
         IF (L .LT. 1) GO TO 500
         DO 400 J = 1, L
            Z(I,J) = ZERO
            Z(J,I) = ZERO
  400    CONTINUE
  500 CONTINUE
      RETURN
        END

!*    SUBROUTINE TQL2 ( NM, N, D, E, Z, IERR )
!**   <TQL2> -- EISPACK ROUTINE -- SEE THEIR WRITEUP

      SUBROUTINE tql2(NM,N,D,E,Z,IERR)
        IMPLICIT        DOUBLE PRECISION                (A-H,O-Z)
        IMPLICIT        INTEGER (I-N)
        DOUBLE PRECISION   D(N),E(N),Z(NM,N),MACHEP
!                MACHEP IS A MACHINE DEPENDENT PARAMETER SPECIFYING
!                THE RELATIVE PRECISION OF FLOATING POINT ARITHMETIC.
!                MACHEP = 2.**(-47) FOR SINGLE PRECISION ARITHMETIC
!                ON CDC 6600-7600
!     DATA MACHEP/16414000000000000000B/
!     DATA MACHEP/ 6E-14/
!                MACHEP = 16**(-13) FOR LONG FORM ARITHMETIC ON IBM370
!     DATA MACHEP / Z3410000000000000 /
!     -- ------- -- --- -------- ------ ----- ---- -- ----- -- --- ----
!     TO CONVERT TO IBM FORTRAN, INSERT ABOVE CARD IN PLACE OF CDC CARD
!     -- ------- -- --- -------- ------ ----- ---- -- ----- -- --- ----
        DATA   MACHEP, ZERO, ONE, TWO  /  3.40282347d-38, 0.0D0,  &
           1.0D0, 2.0d0 /
      IERR = 0
      IF (N .EQ. 1) GO TO 1001
      DO 100 I = 2, N
  100 E(I-1) = E(I)
      F = ZERO
      B = ZERO
      E(N) = ZERO
      DO 240 L = 1, N
         J = 0
         H = MACHEP * (dabs(D(L)) + dabs(E(L)))
         IF (B .LT. H) B = H
!                LOOK FOR SMALL SUB-DIAGONAL ELEMENT
         DO 110 M = L, N
            IF (dabs(E(M)) .LE. B) GO TO 120
!                E(N) IS ALWAYS ZERO, SO THERE IS NO EXIT
!                THROUGH THE BOTTOM OF THE LOOP
  110    CONTINUE
  120    IF (M .EQ. L) GO TO 220
  130    IF (J .EQ. 30) GO TO 1000
         J = J + 1
!                FORM SHIFT
         L1 = L + 1
         G = D(L)
         P = (D(L1) - G) / (TWO * E(L))
         R =  dSQRT(P*P+ONE)
         D(L) = E(L) / (P +  SIGN(R,P))
         H = G - D(L)
         DO 140 I = L1, N
  140    D(I) = D(I) - H
         F = F + H
!                QL TRANSFORMATION
         P = D(M)
         C = ONE
         S = ZERO
         MML = M - L
!                FOR I=M-1 STEP -1 UNTIL L DO --
         DO 200 II = 1, MML
            I = M - II
            G = C * E(I)
            H = C * P
            IF (dabs(P) .LT. dabs(E(I))) GO TO 150
            C = E(I) / P
            R =  dSQRT(C*C+ONE)
            E(I+1) = S * P * R
            S = C / R
            C = ONE / R
            GO TO 160
  150       C = P / E(I)
            R =  dSQRT(C*C+ONE)
            E(I+1) = S * E(I) * R
            S = ONE / R
            C = C * S
  160       P = C * D(I) - S * G
            D(I+1) = H + S * (C * G + S * D(I))
!                FORM VECTOR
            DO 180 K = 1, N
               H = Z(K,I+1)
               Z(K,I+1) = S * Z(K,I) + C * H
               Z(K,I) = C * Z(K,I) - S * H
  180       CONTINUE
  200    CONTINUE
         E(L) = S * P
         D(L) = C * P
         IF (dabs(E(L)) .GT. B) GO TO 130
  220    D(L) = D(L) + F
  240 CONTINUE
!                ORDER EIGENVALUES AND EIGENVECTORS
      DO 300 II = 2, N
         I = II - 1
         K = I
         P = D(I)
         DO 260 J = II, N
            IF (D(J) .GE. P) GO TO 260
            K = J
            P = D(J)
  260    CONTINUE
         IF (K .EQ. I) GO TO 300
         D(K) = D(I)
         D(I) = P
         DO 280 J = 1, N
            P = Z(J,I)
            Z(J,I) = Z(J,K)
            Z(J,K) = P
  280    CONTINUE
  300 CONTINUE
      GO TO 1001
!                SET ERROR -- NO CONVERGENCE TO AN
!                EIGENVALUE AFTER 30 ITERATIONS
 1000 IERR = L
 1001 RETURN
      END


