!--------------------------------------------------------------------------------------------------
! MODULE: ModelMod
!! Ferretti et al. JCP 104, 5517 (1996)
!--------------------------------------------------------------------------------------------------
module modelmod
    use fortutils
    implicit none

    private
    public :: model_ener
    public :: model_nadv
    public :: model_grad

    ! Model parameters (compile time constants)
    real(dp), parameter :: FKx = 0.02_dp
    real(dp), parameter :: FKy = 0.10_dp
    real(dp), parameter :: FDelta = 0.01_dp
    real(dp), parameter :: FX1 = 4.0_dp
    real(dp), parameter :: FX2 = 3.0_dp
    real(dp), parameter :: FX3 = 3.0_dp
    real(dp), parameter :: Falpha = 3.0_dp
    real(dp), parameter :: Fbeta = 1.5_dp
    real(dp), parameter :: Fgamma = 0.01_dp

contains

    subroutine model_ener(x, e)
        real(dp), intent(in) :: x(2)
        real(dp), intent(out) :: e(2)
        real(dp) :: h11, h12, h21, h22

        h11 = FKx / 2 * (x(1) - FX1)**2 + FKy / 2 * x(2)**2
        h22 = FKx / 2 * (x(1) - FX2)**2 + FKy / 2 * x(2)**2 + FDelta
        h12 = Fgamma * x(2) * exp(-Falpha * (x(1) - FX3)**2) * exp(-Fbeta * x(2)**2)
        h21 = h12
        
        e(1) = (h11 + h22 - sqrt(4 * h12 * h12 + (h11 - h22)**2)) / 2
        e(2) = (h11 + h22 + sqrt(4 * h12 * h12 + (h11 - h22)**2)) / 2
    end subroutine model_ener


    subroutine model_grad(cstate, x, gr)
        real(dp), intent(in) :: x(2)
        real(dp), intent(out) :: gr(2)
        integer :: cstate

        real(dp) :: exppart
        real(dp) :: Fcxb, Fcxn, Fcyb, Fcyn

        exppart = exp(-2 * (x(1) - FX3)**2 * Falpha - 2 * x(2)**2 * Fbeta)
        Fcxb = -16 * exppart * x(2)**2 * Fgamma ** 2 * (x(1) - FX3) * Falpha &
             & + FKx * (FX1 - FX2) * (FKx * (2 * x(1) - FX1 - FX2) * (FX1 - FX2) + 2 * FDelta)
        Fcxn =   4 * exppart * x(2)**2 * Fgamma**2 &
             & + ((FKx * (2 * x(1) - FX1 - FX2) * (FX1 - FX2) + 2 * FDelta)**2) / 4
        Fcyb = 4 * exppart * x(2) * (-1 + 2 * x(2)**2 * Fbeta) * Fgamma**2
        Fcyn = Fcxn

        if (cstate == 1) then
            gr(1) = (FKx * (x(1) - FX1) + FKx * (x(1) - FX2) - Fcxb / (2 * sqrt(Fcxn))) / 2
            gr(2) = (2 * FKy * x(2) + Fcyb / sqrt(Fcyn)) / 2
        else
            gr(1) = (FKx * (x(1) - FX1) + FKx * (x(1) - FX2) + Fcxb / (2 * sqrt(Fcxn))) / 2
            gr(2) = (2 * FKy * x(2) - Fcyb / sqrt(Fcyn)) / 2
        end if
    end subroutine model_grad


    subroutine model_nadv(x, e, nadvec)
        real(dp), intent(in) :: x(2)
        real(dp), intent(out) :: e(2)
        real(dp), intent(out) :: nadvec(2, 2, 2)
        real(dp) :: h11, h12, h21, h22
        real(dp) :: dh1(2, 2), dh2(2, 2)
        real(dp) :: vec1(2), vec2(2)
        real(dp) :: tvec(2)


        ! Define 2x2 hamiltonian matrix.
        h11 = FKx / 2 * (x(1) - FX1)**2 + FKy / 2 * x(2)**2
        h22 = FKx / 2 * (x(1) - FX2)**2 + FKy / 2 * x(2)**2 + FDelta
        h12 = Fgamma * x(2) * exp(-Falpha * (x(1) - FX3)**2) * exp(-Fbeta * x(2)**2)
        h21 = h12
    
        e(1) = (h11 + h22 - sqrt(4.0 * h12 * h12 + (h11 - h22)**2)) / 2
        e(2) = (h11 + h22 + sqrt(4.0 * h12 * h12 + (h11 - h22)**2)) / 2
    
        ! Derivative of the hamiltonian matrix with respect to the first coordinate.
        dh1(1,1) = FKx * (x(1) - FX1)
        dh1(2,2) = FKx * (x(1) - FX2)
        dh1(1,2) = - 2 * exp(-(x(1) - FX3)**2 * Falpha - x(2)**2 * Fbeta) * (x(1) - FX3) * x(2) * Falpha * Fgamma
        dh1(2,1) = dh1(1,2)
    
        ! Derivative of the hamiltonian matrix with respect to the second coordinate.
        dh2(1,1) = FKy * x(2)
        dh2(2,2) = FKy * x(2)
        dh2(1,2) = exp(-(x(1) - FX3)**2 * Falpha - x(2)**2 * Fbeta) * Fgamma - &
            &  2 * exp(-(x(1) - FX3)**2 * Falpha - x(2)**2 * Fbeta) * x(2)**2 * Fbeta * Fgamma
        dh2(2,1) = dh2(1,2)
    
        ! Eigenvectors in the adiabatic representation.
        ! The phase of vec2 is corrected to ensure a smooth change with coordinates.
        vec1(1) = 1 / sqrt(1 + ((e(1) - h11) / h12)**2)
        vec1(2) = ((e(1) - h11) / h12) / sqrt(1 + ((e(1) - h11) / h12)**2)
    
        vec2(1) = 1 / sqrt(1 + ((e(2) - h11) / h12)**2)
        vec2(2) = ((e(2) - h11) / h12) / sqrt(1 + ((e(2) - h11) / h12)**2)
        if(x(2) > 0.0_dp) then
            vec2 = - vec2
        endif
    
        nadvec = 0.0_dp
        tvec = matmul(dh1, vec2)
        nadvec(1, 1, 2) = dot_product(vec1,tvec) / (e(1) - e(2))
        tvec = matmul(dh2, vec2)
        nadvec(2, 1, 2) = dot_product(vec1,tvec) / (e(1) - e(2))
    
        nadvec(:, 2, 1) = -nadvec(:, 1, 2)

    end subroutine model_nadv


end module modelmod
