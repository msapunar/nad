module modelmod
    implicit none

    private
    public :: model_ener
    public :: model_grad
    public :: model_mass
    
    real(8), parameter :: pi = 3.1415926535897932d0
    real(8), parameter :: hbar = 1.d0, hbarnew = 1.d0
    real(8), parameter :: au2amu = 5.4857990d-4
    real(8), parameter :: au2ev = 27.211396d0
    real(8), parameter :: au2cm = 219474.63068d0
    real(8), parameter :: au2joule = 4.35974394d-18
    real(8), parameter :: au2fs = 2.418884254d-2
    real(8), parameter :: au2ags = 0.529177249d0
    complex(8), parameter :: eye = (0.d0, 1.d0)

    contains

    subroutine model_ener(geom, ener)
        real(8), intent(in) :: geom(1, 2)
        real(8), intent(out) :: ener(3)
        real(8) :: dia(3, 3)
        real(8) :: vec1(3, 3), vec2(3, 3)
        integer :: ierr
        call phenolpot(geom(1, :), dia)
        call diag(3, 3, dia, ener, vec1, vec2, ierr)
    end subroutine model_ener


    subroutine model_grad(cs, geom, grad)
        integer, intent(in) :: cs
        real(8), intent(in) :: geom(1, 2)
        real(8), intent(out) :: grad(1, 2)

        call deriv_poteng(cs, geom(1, :), grad(1, :))
        grad = - grad
    end subroutine model_grad

    subroutine model_mass(geom,mass)
        real(8), intent(in) :: geom(1, 2)
        real(8), intent(out) :: mass(2)
        call cal_mass(geom(1,:), mass)
    end subroutine model_mass

    subroutine cal_mass(q,mass)
        integer, parameter :: ndim = 2
       
        real(8), intent(in) :: q(ndim)
        real(8), intent(out) :: mass(ndim)
       
        real(8), parameter :: mass_H = 1823.d0, mass_C = 11.9160d0*mass_H, &
                 mass_O = 15.8725d0*mass_H
        real(8), parameter :: alpha = 108.59d0/180.d0*pi, r_CH = 1.0949d0/au2ags, &
                 r_CC = 1.4074d0/au2ags
       
        real(8)  mass_OH, inertia, inertia1, inertia2
       
       
        mass_OH = mass_O*mass_H/(mass_O + mass_H)
       
        inertia1 = mass_OH*q(1)**2*sin(alpha)**2
        inertia2 = 3.d0*mass_C*r_CC**2 + 3.d0*mass_H*(r_CC + r_CH)**2
        inertia = 1.d0/(1.d0/inertia1 + 1.d0/inertia2)
       
        mass(1) = mass_OH
        mass(2) = inertia
    end subroutine cal_mass


    subroutine phenolpot(q,dia)
        integer, parameter :: ndim = 2, ns = 3
        real(8), intent(in) :: q(ndim)
        real(8), intent(out) :: dia(ns,ns)
    ! Constants
        real(8), parameter :: pi = 3.1415926535897932d0
        real(8), parameter :: au2ev = 27.211396d0
        real(8), parameter :: au2ags = 0.529177249d0
    ! All are not in eV !!!
    ! for dia11
        real(8), parameter :: dis1_e = 4.26302d0, r1 = 0.96994d0, &
                 a1 = 2.66021
        real(8), parameter :: ca1 = 0.27037d0, ca2 = 1.96606, &
                 ca3 = 0.685264d0
    ! for dia22
        real(8), parameter :: b201 = 0.192205d0, b211 = -0.2902d0,  &
                 b221 = 27.3756d0, b202 = 5.67356d0, b212 = 2.05715d0,  &
                 b222 = 1.66881d0, b203 = 1.03171d0, b213 = 1.01574, &
                 b223 = 0.20557d0, b204 = 5.50696d0, b214 = -73.329d0, &
                 b224 = 0.35567d0, b205 = 4.70601d0,  b215 = 1.48285d0, & 
                 b225 = 1.43492d0, b206 = 2.49826d0, b216 = -0.1111d0, &
                 b226 = 0.56968d0, b207 = 0.988188d0, b217 = -0.00055d0, &
                 kai22 = 0.d0, b208 = 3.3257d0, kai21 = 0.021105d0, &
                 kai20 = 0.326432d0
!!!! ! for dia33
        real(8), parameter :: dis3_e = 4.47382d0, c1 = 0.110336d0, &
                 r3 = 0.96304d0, c2 = 1.21724d0, a3 = 2.38671d0, &
                 c3 = 0.06778d0, a30 = 4.85842d0 
!!!! ! for coupling term dia12 and dia23
        real(8), parameter :: lam12max = 1.47613d0, d12 = 1.96984d0, &
                 beta12 = 0.494373d0
        real(8), parameter :: lam23max = 0.327204d0, d23 = 1.22594d0, &
                 beta23 = 0.0700604d0
     
        real(8)  v10, v11, v201, v202, v211, v212, v221, v222, &
                 v20, v21, v22, v30, v31
    
        real(8)  lam12, lam23
    
        real(8)  r_OH, tha
    
     
        r_OH = q(1)*au2ags
        tha = q(2)
    ! dia11
        v10 = dis1_e*(1.d0 - exp(- a1*(r_OH - r1)))**2
        v11 = 0.5d0*ca1*(1.d0 - tanh((r_OH - ca2)/ca3))
        dia(1,1) = v10 + v11*(1.d0 - cos(2.d0*tha))
    ! dia22
        v201 = b201*(1.d0 - exp(-b202*(r_OH - b203)))**2 + b204
        v202 = b205*exp(-b206*(r_OH - b207)) + b208
        v211 = 0.5d0*b211*(1.d0 - tanh((r_OH - b212)/b213))
        v212 = 0.5d0*b214*(1.d0 - tanh((r_OH - b215)/b216)) + b217
        v221 = 0.5d0*b221*(1.d0 + tanh((r_OH - b222)/b223))
        v222 = 0.5d0*b224*(1.d0 - tanh((r_OH - b225)/b226))
        v20 = 0.5d0*(v201 + v202) -  &
              0.5d0*sqrt((v201 - v202)**2 + kai20)
        v21 = 0.5d0*(v211 + v212) +  &
              0.5d0*sqrt((v211 - v212)**2 + kai21)
        v22 = 0.5d0*(v221 + v222) -  &
              0.5d0*sqrt((v221 - v222)**2 + kai22)
        dia(2,2) = v20 + v21*(1.d0 - cos(2.d0*tha)) + &
                   v22*(1.d0 - cos(2.d0*tha))**2
    ! dia33
        v30 = dis3_e*(1.d0 - exp(-a3*(r_OH - r3)))**2 + a30
        v31 = 0.5d0*c1*(1.d0 - tanh((r_OH - c2)/c3))
        dia(3,3) = v30 + v31*(1.d0 - cos(2.d0*tha))
    ! dia12, dia13 and dia23 
        lam12 = 0.5d0*lam12max*(1.d0 - tanh((r_OH - d12)/beta12))
        lam23 = 0.5d0*lam23max*(1.d0 - tanh((r_OH - d23)/beta23))
    
        dia(1,2) = lam12*sin(tha)
        dia(2,3) = lam23*sin(tha)
        dia(1,3) = 0.d0
        dia(2,1) = dia(1,2)
        dia(3,2) = dia(2,3)
        dia(3,1) = dia(1,3)
    
        dia = dia/au2ev
     
        return 
    end subroutine phenolpot


    subroutine deriv_poteng(isurf,q,df)
        implicit none
    
        integer, parameter :: ndim = 2, ns = 3
    
        integer, intent(in) :: isurf
    
        real(8), intent(in) :: q(ndim)
        real(8), intent(out) :: df(ndim)
    
        real(8)  q1(ndim), q2(ndim)
        real(8)  dia(ns,ns), adia(ns)
        real(8)  vec1(ns,ns), vec2(ns,ns)
    
        real(8)  f1, f2, dq
    
        integer  id, ierr
    
    
        df = 0.d0; dq = 1.d-3
        do id = 1, ndim
    
           q1 = q; q2 = q
    
           q1(id) = q(id) - dq
           q2(id) = q(id) + dq
    
           call phenolpot(q1,dia)
           call diag(ns, ns, dia, adia, vec1, vec2, ierr)
           f1 = adia(isurf)
    
           call phenolpot(q2,dia)
           call diag(ns, ns, dia, adia, vec1, vec2, ierr)
           f2 = adia(isurf)
    
           df(id) = - (f2 - f1)/2.d0/dq
    
        end do
    
        return
    end subroutine deriv_poteng

end module modelmod
