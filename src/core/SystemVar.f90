!--------------------------------------------------------------------------------------------------
! MODULE: SystemVar
!> @author Marin Sapunar, Ruđer Bošković Institute
!> @date October, 2017
!
! DESCRIPTION: 
!> @brief Defines type for holding all variables about the system in the dynamics calculation.
!--------------------------------------------------------------------------------------------------
module systemvar
    ! Import variables
    use fortutils
    implicit none

    private
    public :: t

    !----------------------------------------------------------------------------------------------
    ! TYPE: TrajType
    !> @brief Holds dynamics variables.
    !----------------------------------------------------------------------------------------------
    type trajtype
        integer :: step = 0 !< Current step.

        ! Full system:
        integer :: ndim = 3 !< Number of dimensions.
        integer :: natom = 0 !< Total number of atoms.
        character(len=2), allocatable :: sym(:) !< List of atom symbols.
        real(dp), allocatable :: chrg(:) !< List of atom partial charges.
        real(dp), allocatable :: mass(:) !< List of atom masses.
        real(dp), allocatable :: masi(:, :) !< Inverted mass matrix.
        real(dp), allocatable :: geom1(:, :) !< Geometry at t.
        real(dp), allocatable :: velo1(:, :) !< Velocity at t.
        real(dp), allocatable :: grad1(:, :) !< Gradient at t.
        real(dp), allocatable :: geom2(:, :) !< Geometry at t + dt.
        real(dp), allocatable :: velo2(:, :) !< Velocity at t + dt.
        real(dp), allocatable :: grad2(:, :) !< Gradient at t + dt.


        ! QM system:
        integer :: qnatom = 0 !< Number of QM atoms.
        integer, allocatable :: qind(:) !< Indexes of the QM atoms in the full system.
        real(dp), allocatable :: qe0(:) !< Potential energies of the QM system at t - dt.
        real(dp), allocatable :: qe1(:) !< Potential energies of the QM system at t.
        real(dp), allocatable :: qe2(:) !< Potential energies of the QM system at t + dt.
        real(dp), allocatable :: qo(:) !< Oscillator strengths of the QM system.

        ! MM system:
        integer :: mnatom = 0 !< Number of MM atoms.
        integer, allocatable :: mind(:) !< Indexes of the MM atoms in the full system.
        real(dp) :: me1(2) !< Last MM energies (1 - QM model sys, 2 - MM sys).
        real(dp) :: me2(2) !< Current MM energies (1 - QM model sys, 2 - MM sys).

        ! Surface hopping variables:
        integer :: nstate = 0 !< Number of states.
        integer :: cstate !< Current state.
        integer :: pstate !< Previous state.
        complex(dp), allocatable :: cwf(:) !< Coefficients of the el. states in the total wf.
        real(dp), allocatable :: prob(:) !< Probabilities of hopping during current step.
        logical, allocatable :: cmask(:,:) !< Selects which elements of the overlap/coupling matrix
                                           !! should be calculated.

        real(dp), allocatable :: olap1(:,:) !< Overlaps between wfs at t - dt and t.
        real(dp), allocatable :: olap2(:,:) !< Overlaps between wfs at t and t + dt.
        real(dp), allocatable :: nadv1(:, :, :) !< Nonadiabatic coupling vectors at t.
        real(dp), allocatable :: nadv2(:, :, :) !< Nonadiabatic coupling vectors at t + dt.

        real(dp) :: pbcbox(6)
    contains
        procedure :: prepnext => traj_prepnext
        procedure :: writestep => traj_writestep
        procedure :: tote => traj_tote !< Total energy of the system.
        procedure :: kine => traj_kine !< Kinetic energy of the system.
        procedure :: pote => traj_pote !< Potential energy of the system.
        procedure :: qkine => traj_qkine !< QM kinetic energy.
        procedure :: mkine => traj_mkine !< MM kinetic energy.
    end type trajtype

    type(trajtype) :: t

contains

    pure function traj_tote(t) result(tote)
        class(trajtype), intent(in) :: t
        real(dp) :: tote
        tote = t%pote() + t%kine()
    end function traj_tote

    pure function traj_pote(t) result(pote)
        class(trajtype), intent(in) :: t
        real(dp) :: pote
        pote = t%qe2(t%cstate) + t%me2(2) - t%me2(1)
    end function traj_pote

    pure function traj_kine(t) result(kine)
        use energymod, only : ekin
        class(trajtype), intent(in) :: t
        real(dp) :: kine
        kine = ekin(t%mass, t%velo2)
    end function traj_kine

    pure function traj_qkine(t) result(qkine)
        use energymod, only : ekin
        class(trajtype), intent(in) :: t
        real(dp) :: qkine
        qkine = ekin(t%mass(t%qind), t%velo2(:, t%qind))
    end function traj_qkine

    pure function traj_mkine(t) result(mkine)
        use energymod, only : ekin
        class(trajtype), intent(in) :: t
        real(dp) :: mkine
        mkine = ekin(t%mass(t%mind), t%velo2(:, t%mind))
    end function traj_mkine


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: Traj_PrepNext
    !
    ! DESCRIPTION:
    !> @brief Overwrite values from previous step with values from current step.
    !----------------------------------------------------------------------------------------------
    subroutine traj_prepnext(t)
        class(trajtype) :: t

        t%geom1 = t%geom2
        t%velo1 = t%velo2
        t%grad1 = t%grad2

        if (allocated(t%olap1)) t%olap1 = t%olap2
        if (allocated(t%nadv1)) t%nadv1 = t%nadv2

        t%qe0 = t%qe1
        t%qe1 = t%qe2
        t%me1 = t%me2
    end subroutine traj_prepnext


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: Traj_WriteStep
    !
    ! DESCRIPTION:
    !> @brief Write requested output from each step.
    !----------------------------------------------------------------------------------------------
    subroutine traj_writestep(t, ounit, popt)
        class(trajtype) :: t !< Trajectory variable.
        integer, intent(in) :: ounit !< Output unit.
        logical, intent(in) :: popt(:) !< Print options.
        integer :: i

         
        write(ounit, '(a, 1x, i0)') '0. Step:', t%step
        if (popt(1)) then
            write(ounit, '(a, 1x, i0)') '1. Current electronic state: ', t%cstate
        end if
        if (popt(2)) then
            write(ounit, '(a)') '2. Coordinates: '
            do i = 1, t%natom
                write(ounit, 1002) t%sym(i), t%geom2(:, i)
            end do
        end if
        if (popt(3)) then
            write(ounit, '(a)') '3. Velocities: '
            do i = 1, t%natom
                write(ounit, 1002) t%sym(i), t%velo2(:, i)
            end do
        end if
        if (popt(4)) then
            write(ounit, '(a)') '4. Gradients: '
            do i = 1, t%natom
                write(ounit, 1002) t%sym(i), t%grad2(:, i)
            end do
        end if
        if (popt(5)) then
            write(ounit, '(a)') '5. Energies: '
            write(ounit, 1005) t%tote(), t%pote(), t%kine()
            write(ounit, 1005) t%qe2(t%cstate), t%qkine()
            if (t%mnatom > 0) write(ounit, 1005) t%me2(2) - t%me2(1), t%mkine()
        end if
        if (popt(6)) then
            write(ounit, '(a)') '6. QM state energies: '
            write(ounit, 1006) t%qe2(1:t%nstate)
        end if
        if (popt(7)) then
            write(ounit, '(a)') '7. Wavefunction coefficients: '
            write(ounit, 1006) real(t%cwf(:))
            write(ounit, 1006) aimag(t%cwf(:))
        end if
        if (popt(8)) then
            write(ounit, '(a)') '8. Hopping probabilities: '
            write(ounit, 1006) t%prob(1:t%nstate)
        end if
        if (popt(9)) then
            write(ounit, '(a)') '9. Overlap matrix: '
            do i = 1, t%nstate
                write(ounit, 1006) t%olap2(i, 1:t%nstate)
            end do
        end if
        if (popt(11)) then
            write(ounit, '(a)') '11. QM oscillator strengths: '
            write(ounit, 1005) t%qo(1:t%nstate - 1)
        end if

1002 format (1x,a2,2x,1000f20.12) 
1005 format (1x,1000f20.12)
1006 format (1x,1000e22.12)

    end subroutine traj_writestep

end module systemvar
