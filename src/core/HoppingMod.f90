!--------------------------------------------------------------------------------------------------
! MODULE: HoppingMod
!> @author Marin Sapunar, Ruđer Bošković Institute
!> @date November, 2016
!
! DESCRIPTION:
!> @brief Surface hopping algorithm.
!--------------------------------------------------------------------------------------------------
module hoppingmod
    use fortutils
    implicit none

    private
    public :: hopping
    public :: sh_rescalevelo
    public :: sh_frustratedhop

contains


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: Hopping
    !
    ! DESCRIPTION:
    !> @brief Selection of surface hopping algorithm.
    !> @details
    !! Calls appropriate subroutine for surface hopping depending on the ctrl%sh variable. Also
    !! calls the decoherence correction subroutine and phase matching subroutine.
    !----------------------------------------------------------------------------------------------
    subroutine hopping()
        use systemvar
        use controlvar
        use phasemod
        use decoherencemod
        integer :: tst

        tst = t%cstate
        select case(ctrl%sh)
        case(1)
            if (t%step > 1) then
                call sh_lz(ctrl%dt, t%qe0, t%qe1, t%qe2, tst)
            end if
        case(2)
            call decoherence()
            call phasematch()
            call sh_adiabatic(ctrl%tdc_type, ctrl%ene_interpolate, ctrl%tdc_interpolate,           &
            &                 ctrl%tdc_interpolate, ctrl%dt, ctrl%shnstep, t%qe1, t%qe2, t%cwf,    &
            &                 tst, t%olap1, t%olap2, t%nadv1, t%nadv2,                             &
            &                 t%velo1(:, t%qind), t%velo2(:, t%qind), t%prob)
        case(3)
            call decoherence()
            call phasematch()
            call sh_diabatic(t%nstate, ctrl%dt, t%qe1, t%qe2, t%cwf, tst, t%olap2, t%prob)
        end select

        if (tst /= t%cstate) ctrl%hop = .true.
        t%pstate = t%cstate
        t%cstate = tst
    end subroutine hopping


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: SH_Diabatic
    !
    ! DESCRIPTION:
    !> @brief Fewest switches surface hopping in a locally diabatic basis.
    !> @details
    !! Propagates electronic wave function coefficients and determines hops for the SH method.
    !----------------------------------------------------------------------------------------------
    subroutine sh_diabatic(n, dt, qe1, qe2, cwf, tst, olp, fprob)
        use matrixmod, only : genmat_diag, &
                              matexp_sy
        use orthomod, only : orth_lowdin
        use blas95, only : gemm
        integer, intent(in) :: n !< Number of states.
        real(dp), intent(in) :: dt !< Nuclear dynamics time step.
        real(dp), intent(in) :: olp(n, n) !< Overlap matrix for current step.
        real(dp), intent(in) :: qe1(n) !< Energies at t.
        real(dp), intent(in) :: qe2(n) !< Energies at t + dt.
        complex(dp), intent(inout) :: cwf(n) !< WF coefficients in the adiabatic basis.
        integer, intent(inout) :: tst !< Current state.
        real(dp), intent(out) :: fprob(n) !< Final probability for each state.
        real(dp) :: t(n, n) !< Orthogonalized overlap matrix.
        real(dp) :: w1(n, n) !< Work array 1.
        real(dp) :: w2(n, n) !< Work array 2.
        complex(dp) :: u(n, n) !< Transformation matrix.
        complex(dp) :: w3(n, n) !< Work array 3.
        complex(dp) :: w4(n, n) !< Work array 4.
        complex(dp) :: pwf(n) !< Previous WF coefficients.
        real(dp) :: b(n) !< Contributions of each state to change in population.
        real(dp) :: rnum !< Random number for surface hopping.
        real(dp) :: denom !< Denominator in calculation of b.
        real(dp), parameter :: thresh = 1.e-10_dp !< Threshold for denominator.
        real(dp) :: cprob !< Cumulative probability of hopping into any state.
        integer :: k, l

        t = olp
        pwf = cwf

        ! Orthogonalize overlap matrix.
        call orth_lowdin(t)

        ! Generate Z matrix. (Approx. Hamiltonian at half step.)
        call gemm(t, genmat_diag(qe2), w1)
        call gemm(w1, t, w2, transb='T') ! H(t+dt/2) = T.E(t+dt).Tt
        w2 = (genmat_diag(qe1) + w2) * 0.5_dp ! Z = (E(0) + H(t+dt))/2

        ! Generate U matrix. (Transformation matrix.)
        w3 = matexp_sy(w2, cmplx(0.0_dp, -dt, kind=dp)) ! exp(-i * Z * dt)
        w4 = cmplx(t, 0.0_dp, dp) !< Convert T to complex matrix.
        call gemm(w4, w3, u, transa='T') ! u = T^t exp(-i * Z * dt)

        ! Calculate new wf coefficients.
        cwf = matmul(u, pwf) ! A(t0 + dt) = U A(t0)

        ! Calculate hopping probabilities.
        k = tst
        b = 0.0_dp
        denom = abs(cwf(k))**2 - real(u(k, k) * pwf(k) * conjg(cwf(k)))
        if (denom > thresh) then
            denom = (abs(cwf(k))**2 - abs(pwf(k))**2) / denom
            do l = 1, n
                if (l == k) cycle
                b(l) = - real(u(k, l) * pwf(l) * conjg(cwf(k))) * denom
            end do
        else
            ! If the denominator is close to zero, the property b(k, l) = - b(l, k) is used.
            do l = 1, n
                if (l == k) cycle
                denom = abs(cwf(l))**2 - real(u(l, l) * pwf(l) * conjg(cwf(l)))
                if (denom < thresh) cycle
                denom = (abs(cwf(l))**2 - abs(pwf(l))**2) / denom
                b(l) = real(u(l, k) * pwf(k) * conjg(cwf(l))) * denom
            end do
        end if
        b = b / abs(pwf(k))**2

        ! Determine if hop should occur.
        call random_number(rnum)
        cprob = 0.0_dp
        fprob = 0.0_dp
        hop: do l = 1, n
            if (l == k) cycle
            if (b(l) > 0.0_dp) then ! Not actual probability, can be negative.
                cprob = cprob + b(l)
                fprob(l) = fprob(l) + b(l)
                if (rnum < cprob) then
                    tst = l
                    exit hop
                end if
            end if
        end do hop
    end subroutine sh_diabatic


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: SH_Adiabatic
    !
    ! DESCRIPTION:
    !> @brief Tully's Fewest Switches Surface Hopping Method.
    !> @details
    !! Propagates electronic wave function coefficients and determines hops for the FSSH method.
    !! The coefficients are propagated using an external subroutine for solving the system of
    !! ordinary differential equations.
    !! The nuclear time step (dt) is split into smaller time steps for which the coefficients and
    !! hopping probabilities are calculated.
    !----------------------------------------------------------------------------------------------
    subroutine sh_adiabatic(opt_clvl, opt_inte, opt_into, opt_intv, dt, nstep, qe1, qe2, cwf,   &
    &                         tst, olp1, olp2, nadv1, nadv2, vel1, vel2, fprob)
        use odemod ! Interface to Shampine/Gordon ODE solver.
        integer, intent(in) :: opt_clvl !< Method for calculating time-derivative couplings.
        integer, intent(in) :: opt_inte !< Method for interpolating energies during the time step.
        integer, intent(in) :: opt_into !< Method for interpolating overlaps during the time step.
        integer, intent(in) :: opt_intv !< Method for interpolating nad vecs during the time step.
        real(dp), intent(in) :: dt !< Nuclear dynamics time step.
        integer, intent(in) :: nstep !< Number of substeps.
        real(dp), intent(in) :: qe1(:) !< Energies at t0.
        real(dp), intent(in) :: qe2(:) !< Energies at t0 + dt.
        complex(dp), intent(inout) :: cwf(:) !< WF coefficients.
        integer, intent(inout) :: tst !< Current state.
        real(dp), intent(in) :: olp1(:, :) !< Overlap matrix between wfs at t0 - dt and t0.
        real(dp), intent(in) :: olp2(:, :) !< Overlap matrix between wfs at t0 and t0 + dt.
        real(dp), intent(in) :: nadv1(:, :, :) !< Nonadiabatic coupling vectors at t0.
        real(dp), intent(in) :: nadv2(:, :, :) !< Nonadiabatic coupling vectors at t0 + dt.
        real(dp), intent(in) :: vel1(:, :) !< Velocities at t0.
        real(dp), intent(in) :: vel2(:, :) !< Velocities at t0 + dt.
        real(dp), intent(out) :: fprob(:) !< Final probability for each state.
        real(dp) :: edt !< Time step for the propagation of the electronic WF.
        real(dp) :: tt !< Current time during propagation.
        real(dp) :: prob !< Probability of hopping into a state.
        real(dp) :: cprob !< Cumulative probability of hopping into any state.

        integer :: i
        integer :: st
        integer :: de_flag
        real(dp) :: rnum !< Random number for surface hopping.

        odens = size(qe1)
        allocate(odeen(odens))
        allocate(odecmat(odens, odens))

        ! Propagation time step.
        tt = 0.0_dp
        edt = dt / nstep


        fprob = 0.0_dp
        do i = 1, nstep
            ! Get energies and TDCs for current substep.
            call sh_interpolate_energy(opt_inte, nstep, i, qe1, qe2, odeen)
            select case(opt_clvl)
            case(1)
                call sh_interpolate_overlap(opt_into, nstep, i, dt, olp1, olp2, odecmat)
            case(2)
                call sh_interpolate_nadvec(opt_intv, nstep, i, dt, nadv1, nadv2, vel1, vel2, odecmat)
            end select

            ! Propagate wf coefficients.
            call callode(odens, cwf, tt, edt, de_flag)

            ! Determine hopping probabilities.
            call random_number(rnum)
            cprob = 0.0_dp
            hop: do st = 1, odens
                if (st == tst) cycle
                prob = - 2 * edt * odecmat(st, tst) * real(conjg(cwf(st)) * cwf(tst)) / &
                     & (abs(cwf(tst))**2)
                if (prob > 0.0_dp) then ! Not actual probability, can be negative.
                    cprob = cprob + prob
                    fprob(st) = fprob(st) + prob
                    if (rnum < cprob) then
                        tst = st
                        exit hop
                    end if
                end if
            end do hop
        end do

        deallocate(odeen)
        deallocate(odecmat)
    end subroutine sh_adiabatic


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: sh_lz
    !
    ! DESCRIPTION:
    !> @brief Determine jump based on Landau-Zener formula.
    !----------------------------------------------------------------------------------------------
    subroutine sh_lz(dt, qe0, qe1, qe2, cstate)
        use constants, only : hpi
        real(dp), intent(in) :: dt !< Time step.
        real(dp), intent(in) :: qe0(:) !< Energies at t - 2dt.
        real(dp), intent(in) :: qe1(:) !< Energies at t - dt.
        real(dp), intent(in) :: qe2(:) !< Energies at t.
        integer, intent(inout) :: cstate !< Current state.
        real(dp) :: g0, g1, g2 !< Energy gaps at different time steps.
        real(dp) :: sd !< Second derivative of the gap.
        real(dp) :: pr !< Probability of hop to state i.
        integer :: sel !< State with maximum hop probability.
        real(dp) :: prob !< Probability of hop.
        real(dp) :: rnum !< Random number.
        integer :: i !< Iterator.

        sel = -1
        prob = -1.0_dp
        do i = 1, size(qe0)
            if (i == cstate) cycle
            g0 = qe0(cstate) - qe0(i)
            g1 = qe1(cstate) - qe1(i)
            g2 = qe2(cstate) - qe2(i)
            if ((abs(g1) < abs(g2)) .and. (abs(g1) < abs(g0))) then
                sd = (g2 - 2 * g1 + g0) / dt ** 2
                pr = exp(- hpi * sqrt(g1 ** 3 / sd))
                if (pr > prob) then
                    sel = i
                    prob = pr
                end if
            end if
        end do

        if (sel < 0) return  ! Return if no states meet criteria.
        call random_number(rnum)
        if (prob > rnum) cstate = sel
    end subroutine sh_lz



    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: SH_Interpolate_Energy
    !
    ! DESCRIPTION:
    !> @brief Energy interpolation during single nuclear time step in surface hopping.
    !> @details
    !! Returns electronic state energies during the propagation of wave function coefficients in a
    !! surface hopping calculation. The interpolation is done based on the calculated energies at
    !! the beginning and end of the time nuclear time step, E(t) and E(t + dt).
    !!
    !! The behaviour of the subroutine is determined by the value of opt:
    !! - 0 - At all substeps, the energy is constant and equal to the E(t + dt).
    !! - 1 - The energies are equal to E(t) until t + dt/2, and to E(t + dt) afterwards.
    !! - 2 - The energies are a linear interpolation between E(t) and E(t + dt).
    !----------------------------------------------------------------------------------------------
    subroutine sh_interpolate_energy(opt, ni, i, en1, en2, ien)
        integer, intent(in) :: opt !< Type of interpolation.
        integer, intent(in) :: ni !< Number of electronic time steps.
        integer, intent(in) :: i !< Current electronic time step.
        real(dp), intent(in) :: en1(:) !< Energies in previous nuclear time step.
        real(dp), intent(in) :: en2(:) !< Energies in current nuclear time step.
        real(dp), intent(out) :: ien(:) !< Interpolated energies.

        select case(opt)
        ! Constant value.
        case(0)
            if (i == 1) ien = en2
        ! Constant with step in middle.
        case(1)
            if (i < ni / 2) then
                ien = en1
            else
                ien = en2
            end if
        ! Linear interpolation.
        case(2)
            ien = en1 + (en2 - en1) * (i - 1) / ni
        end select
    end subroutine sh_interpolate_energy


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: SH_Interpolate_Overlap
    !
    ! DESCRIPTION:
    !> @brief TDC interpolation during single nuclear time step in surface hopping.
    !> @details
    !! Returns time-derivative couplings during the propagation of wave function coefficients in a
    !! surface hopping calculation. The interpolation is done based on the overlaps of the wfs at
    !! the beginning and end of the nuclear time step.
    !!
    !! The behaviour of the subroutine is determined by the value of opt:
    !! - 1 - Finite differences method to calculate tdc at \f$ t_0 + dt/2 \f$.
    !! - 2 - Finite differences method to calculate tdc at \f$ t_0 - dt/2 \f$ and \f$ t_0 + dt/2 \f$
    !!       and then use linear inter(extra)polation between \f$ t_0 \f$ and \f$ t_0 + dt \f$.
    !! - 4 - Norm-preserving interpolation of Meek and Levine. (10.1021/jz5009449).
    !----------------------------------------------------------------------------------------------
    subroutine sh_interpolate_overlap(opt, ni, i, dt, olp1, olp2, itdc)
        use tdcmod
        integer, intent(in) :: opt !< Type of interpolation.
        integer, intent(in) :: ni !< Number of electronic time steps.
        integer, intent(in) :: i !< Current electronic time step.
        real(dp), intent(in) :: dt !< Nuclear time step.
        real(dp), intent(in) :: olp1(:, :) !< Overlaps between wfs at t0 - dt and t0.
        real(dp), intent(in) :: olp2(:, :) !< Overlaps between wfs at t0 and t0 + dt.
        real(dp), allocatable, intent(inout) :: itdc(:, :) !< Time-derivative couplings.
        real(dp), allocatable, save :: prev(:, :) !< TDCs at t0 - dt/2.
        real(dp), allocatable, save :: crnt(:, :) !< TDCs at t0 + dt/2.

        
        select case(opt)
        ! Constant value.
        case(1)
            if (i == 1) call overlap2tdc(dt, olp2, itdc)
        ! Linear interpolation using coupling matrix.
        case(2)
            if (i == 1) then
                call overlap2tdc(dt, olp1, prev)
                call overlap2tdc(dt, olp2, crnt)
            end if
            itdc = prev + (crnt - prev) * (0.5_dp + (i - 1) / ni)
        ! Norm-preserving interpolation (based on 10.1021/jz5009449).
        case(4)
            if (i == 1) call npi_tdc_integrated(dt, olp2, itdc)
        end select
    end subroutine sh_interpolate_overlap
    

    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: SH_Interpolate_NadVec
    !
    ! DESCRIPTION:
    !> @brief TDC interpolation during single nuclear time step in surface hopping.
    !> @details
    !! Returns time-derivative couplings during the propagation of wave function coefficients in a
    !! surface hopping calculation. The interpolation is done based on the nonadiabatic coupling 
    !! vectors at the beginning and end of the nuclear time step.
    !!
    !! The behaviour of the subroutine is determined by the value of opt:
    !! - 1 - Constant TDCs at t0 + dt.
    !! - 2 - Linear interpolation between TDCs at t0 and at t0 + dt.
    !----------------------------------------------------------------------------------------------
    subroutine sh_interpolate_nadvec(opt, ni, i, dt, nadv1, nadv2, vel1, vel2, itdc)
        use tdcmod
        integer, intent(in) :: opt !< Type of interpolation.
        integer, intent(in) :: ni !< Number of electronic time steps.
        integer, intent(in) :: i !< Current electronic time step.
        real(dp), intent(in) :: dt !< Nuclear time step.
        real(dp), intent(in) :: nadv1(:, :, :) !< Coupling vectors at t0.
        real(dp), intent(in) :: nadv2(:, :, :) !< Coupling vectors at t0 + dt.
        real(dp), intent(in) :: vel1(:, :) !< Velocities at t0.
        real(dp), intent(in) :: vel2(:, :) !< Velocities at t0 + dt.
        real(dp), intent(inout) :: itdc(:, :) !< Time-derivative couplings.
        real(dp), allocatable, save :: prev(:, :) !< TDCs at t0.
        real(dp), allocatable, save :: crnt(:, :) !< TDCs at t0 + dt.

        
        select case(opt)
        ! Constant value.
        case(1)
            if (i == 1) call nadvec2tdc(nadv2, vel2, itdc)
        ! Linear interpolation using coupling matrix.
        case(2)
            if (i == 1) then
                call nadvec2tdc(nadv1, vel1, prev)
                call nadvec2tdc(nadv2, vel2, crnt)
            end if
            itdc = prev + (crnt - prev) * (i - 1) / ni
        end select
    end subroutine sh_interpolate_nadvec
 

    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: SH_RescaleVelo
    !
    ! DESCRIPTION:
    !> @brief
    !> @details
    !----------------------------------------------------------------------------------------------
    subroutine sh_rescalevelo(opt, amask, mass, ppe, cpe, cgrd, pgrd, velo, flag)
        use energymod, only : ekin
        integer, intent(in) :: opt !< Type of momentum correction.
        integer, intent(in) :: amask(:) !< Atoms considered when rescaling.
        real(dp), intent(in) :: ppe !< Previous state potential energy.
        real(dp), intent(in) :: cpe !< Current state potential energy.
        real(dp), intent(in) :: mass(:) !< Masses.
        real(dp), intent(in) :: pgrd(:, :) !< Gradient on previous state.
        real(dp), intent(in) :: cgrd(:, :) !< Gradient on current state.
        real(dp), intent(inout) :: velo(:, :) !< Velocities.
        integer, intent(out) :: flag !< 0 for successful hop, 1 for rejected hop.
        real(dp) :: crit !< Available energy for rescaling.
        real(dp) :: ke !< Kinetic energy.
        real(dp) :: vscale !< Scaling factor.
        real(dp) :: v(size(velo, 1), size(amask)) !< Temporary velocity array.
        real(dp) :: m(size(amask)) !< Temporary mass array.

        ! Work with temporary arrays.
        v = velo(:, amask)
        m = mass(amask)

        flag = 1
        select case(opt)
        case(0) ! No rescaling.
        case(1) ! Rescale along velocity vector.
            ke = ekin(m, v)
            crit = ke + ppe - cpe
            if (crit < 0) return
            vscale = sqrt(crit / ke)
            v = v * vscale
            flag = 0
        case(2) ! Rescale along gradient difference vector.
            !> @todo Add rescaling along gradient difference vector.
            stop 'gdiff rescale not implemented'
        case(3) ! Rescale along nonadiabatic coupling vector.
            !> @todo Add rescaling along nonadiabatic coupling vector.
            stop 'nadvec rescale not implemented'
        end select

        ! Save changes to velocity array.
        velo(:, amask) = v
    end subroutine sh_rescalevelo


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: SH_FrustratedHop
    !
    ! DESCRIPTION:
    !> @brief Return to previous state if a hop is rejected.
    !> @details
    !! The behaviour of the subroutine is determined by the value of opt:
    !! - 1 - Return to previous state without changing velocity.
    !! - 2 - Return to previous state, but also invert the velocity along the relevant direction.
    !----------------------------------------------------------------------------------------------
    subroutine sh_frustratedhop(opt, pgrd, cgrd, pst, cst)
        integer, intent(in) :: opt !< Method of treating frustrated hops.
        real(dp), intent(in) :: pgrd(:, :) !< Gradient on previous state. (New current gradient.)
        real(dp), intent(inout) :: cgrd(:, :) !< Gradient on current state.
        integer, intent(in) :: pst !< Previous state. (New current state.)
        integer, intent(inout) :: cst !< Current state.

        select case(opt)
        case(1) ! Just return to previous state. 
            cgrd = pgrd
            cst = pst
        case(2) ! Invert velocity along rescale direction.
            !> @todo Implement velocity inversion on frustrated hop.
            stop 'Inversion on frustrated hop not implemented.'
        end select
    end subroutine sh_frustratedhop
 

end module hoppingmod
