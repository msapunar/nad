!--------------------------------------------------------------------------------------------------
! MODULE: NuclearDynMod
!
! DESCRIPTION: 
!> @brief Subroutines for propagation of nuclear coordinates.
!--------------------------------------------------------------------------------------------------
module nucleardynmod
    ! Import variables
    use fortutils
    implicit none

    private
    public :: dyn_updategeom
    public :: dyn_updatevelo

contains


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: Dyn_UpdateVelo
    !
    ! DESCRIPTION:
    !> @brief Update geometry using the velocity Verlet algorithm.
    !> @details
    !! When updating the geometry, the rattle algorithm is called to ensure any constraints are
    !! satisfied.
    !----------------------------------------------------------------------------------------------
    subroutine dyn_updategeom(dt, masi, geo1, grad, velo, geo2, cnstr)
        use rattlemod
        real(dp), intent(in) :: dt
        real(dp), intent(in) :: masi(:, :)
        real(dp), intent(in) :: geo1(:, :)
        real(dp), intent(in) :: grad(:, :)
        real(dp), intent(in) :: velo(:, :)
        real(dp), intent(out) :: geo2(:, :)
        type(constraint), allocatable, intent(in) :: cnstr(:)
        real(dp) :: q(size(geo2, 1), size(geo1, 2))

        q = velo - dt * matmul(grad, masi) * 0.5_dp
        if (allocated(cnstr)) call rattle_geom(cnstr, dt, geo1, masi, q)
        geo2 = geo1 + dt * q
    end subroutine dyn_updategeom


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: Dyn_UpdateVelo
    !
    ! DESCRIPTION:
    !> @brief Update velocity using the velocity Verlet algorithm.
    !> @details
    !! After updating the velocity, the rattle algorithm is called to ensure any constraints are
    !! satisfied.
    !----------------------------------------------------------------------------------------------
    subroutine dyn_updatevelo(dt, masi, geom, grd1, grd2, vel1, vel2, cnstr)
        use rattlemod
        real(dp), intent(in) :: dt
        real(dp), intent(in) :: masi(:, :)
        real(dp), intent(in) :: geom(:, :)
        real(dp), intent(in) :: grd1(:, :)
        real(dp), intent(in) :: grd2(:, :)
        real(dp), intent(in) :: vel1(:, :)
        real(dp), intent(out) :: vel2(:, :)
        type(constraint), allocatable, intent(in) :: cnstr(:)
        vel2 = vel1 - dt * matmul(grd1 + grd2, masi) * 0.5_dp
        if (allocated(cnstr)) call rattle_velo(cnstr, geom, masi, vel2)
    end subroutine dyn_updatevelo


end module nucleardynmod
