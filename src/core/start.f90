!--------------------------------------------------------------------------------------------------
!> @mainpage Dynamics
!> Trajectory surface-hopping nonadiabatic dynamics program
!--------------------------------------------------------------------------------------------------
! PROGRAM: Start
!> @author Marin Sapunar, Ruđer Bošković Institute
!> @date November, 2016
!
! DESCRIPTION: 
!> @brief Trajectory surface-hopping nonadiabatic dynamics program.
!> @details 
!! Main program file. Calls input and output subroutines and contains the main dynamics loop. 
!! Interfaces to QM and MM programs are called from within the loop to calculate energies, 
!! gradients and couplings of the system. Once the calculations are performed, the electronic 
!! wavefunction and nuclear coordinates are propagated.
!--------------------------------------------------------------------------------------------------
program start
    ! Import variables
    use fortutils
    use controlvar
    use systemvar

    ! Import subroutines
    use clockmod
    use randommod
    use backupmod
    use inputmod
    use interfacemod
    use hoppingmod
    use nucleardynmod
    implicit none

    logical :: check
    integer :: flag
    type(clocktype) :: mainclock
    type(clocktype) :: stepclock

    call mainclock%start()

    write(stdout, *) '-------------------------------------------------------------------------------'
    write(stdout, *) '                               Name, version                                   '
    write(stdout, *) 'Program compiled '//__DATE__//' at '//__TIME__//'.'
    write(stdout, *) '-------------------------------------------------------------------------------'

    ! Read input file(s)
    call readinput()

    ! Start the random number generator.
    write(stdout, *)
    call init_random_seed(ctrl%seed(1))
    write(stdout, '(a,i0)') 'Random number generator started with seed: ', ctrl%seed

    ! Restart old trajectory or open new output file.
    if (ctrl%restart) then
        ! Restart old trajectory.
        call readbackup()
    else
        ! Open main output file.
        inquire(file=ctrl%outfile, exist=check)
        if (check) then
            write(stderr,*) 'Error. Output file already present in directory.'
            write(stderr,*) '  Delete file or use the $restart keyword.'
            stop
        end if
        open(newunit=ctrl%ounit, file=ctrl%outfile)

        ! Run energy/gradient calculation for initial geometry.
        if (ctrl%mm) then
            write(stdout, '(a)') 'Running initial MM calculation: '
            call stepclock%start()
            call interface_mmrun()
            call stepclock%print(stdout, '  MM run time:')
        end if
        write(stdout, '(a)') 'Running initial QM calculation: '
        call stepclock%start()
        call interface_qmrun()
        call stepclock%print(stdout, '  QM run time:')

        call t%writestep(ctrl%ounit, ctrl%print)
        call t%prepnext()
        t%step = t%step + 1
    end if


    write(stdout, *)
    write(stdout, *) '-------------------------------------------------------------------------------'
    write(stdout, *) '                          STARTING MAIN PROGRAM LOOP                           '
    write(stdout, *)

    main: do while (t%step <= ctrl%nstep)
        call stepclock%start()
        write(stdout, '(1x,a,i0)') 'Starting step: ', t%step

        ! Get new geometry.
        call dyn_updategeom(ctrl%dt, t%masi, t%geom1, t%grad1, t%velo1, t%geom2, ctrl%cns)

        ! Get new gradients.
        if (ctrl%mm) call interface_mmrun()
        call interface_qmrun()

        ! Get new velocity.
        call dyn_updatevelo(ctrl%dt, t%masi, t%geom2, t%grad1, t%grad2, t%velo1, t%velo2, ctrl%cns)

        ! Integrate TDSE.
        call hopping()
        if (ctrl%hop) then
            write(stdout, '(3x,a)') 'Change of state: '
            write(stdout, '(5x,a,i0)') 'Previous state: ', t%pstate
            write(stdout, '(5x,a,i0)') 'Current state: ', t%cstate
            write(stdout, '(5x,a)') 'Running QM gradient calculation for new state.'
            t%grad1 = t%grad2 ! Overwrites gradient from last step!
            call interface_qmrun()
            call sh_rescalevelo(ctrl%vrescale, t%qind, t%mass, t%qe2(t%pstate), t%qe2(t%cstate),   &
            &                   t%grad1, t%grad1, t%velo2, flag)
            if (flag == 1) call sh_frustratedhop(ctrl%fhop, t%grad1, t%grad2, t%pstate, t%cstate)
        end if
        
        ! Write output and prepare for next step.
        call t%writestep(ctrl%ounit, ctrl%print)
        call t%prepnext()
        ctrl%hop = .false.
        if (mod(t%step, ctrl%buinterval) == 0) call writebackup()

        ! Stop the program if the excited state calculation gives negative excitation energies.
        ! (For example at a S0/S1 conical intersection for Turbomole ADC(2) calculations.)
        if (t%nstate > 1) then
            if (t%qe2(2) - t%qe2(1) < ctrl%min01gap) then
                write(stdout, *) 'Intersection with ground state detected, ending calculation.'
                write(stdout, *)
                ctrl%abort = .true.
            end if
        end if

        ! The program can be stopped after completing a step by creating a 'dynamics.stop' file
        ! in the working directory.
        inquire(file='dynamics.stop', exist=check)
        if (check) then
            write(stdout, *) 'Stop file found in working directory, ending calculation.'
            write(stdout, *)
            ctrl%abort = .true.
            call system('rm dynamics.stop')
        end if

        if (ctrl%abort) then
            call writebackup()
            exit main
        end if

        t%step = t%step + 1
        call stepclock%print(stdout, '   Step run time:')
    end do main


    write(stdout, *)
    write(stdout, *) '                         FINISHING MAIN PROGRAM LOOP                           '
    write(stdout, *) '-------------------------------------------------------------------------------'
    write(stdout, *)

    call mainclock%print(stdout, ' Total run time:')


end program start
