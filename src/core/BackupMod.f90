!--------------------------------------------------------------------------------------------------
! MODULE: BackupMod
!> @author Marin Sapunar, Ruđer Bošković Institute
!> @date December, 2016
!
! DESCRIPTION: 
!> @brief  Write and read a backup file from which the trajectory can be restarted.
!--------------------------------------------------------------------------------------------------
module backupmod
    use fortutils
    use systemvar
    use controlvar
    implicit none

    private
    public :: writebackup
    public :: readbackup

contains

    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: WriteBackup
    !
    ! DESCRIPTION:
    !> @brief Write all variables required to contuinue a dynamics calculation to the backup file.
    !----------------------------------------------------------------------------------------------
    subroutine writebackup
        integer :: i
        integer :: ounit
        character(len=*), parameter :: frmt = '(1x,1000es23.16)'
         
        open(newunit=ounit, file=ctrl%bufile)
        write(ounit, '(i0)') t%step
        do i = 1, t%natom
            write(ounit, frmt) t%geom1(:, i)
        end do
        do i = 1, t%natom
            write(ounit, frmt) t%velo1(:, i)
        end do
        do i = 1, t%natom
            write(ounit, frmt) t%grad1(:, i)
        end do

        write(ounit, '(i0)') t%cstate
        write(ounit, frmt) t%qe1(1:t%nstate)
        if (ctrl%sh == 1) then
            write(ounit, frmt) t%qe0(1:t%nstate)
        else if (ctrl%sh >= 2) then
            do i = 1, t%nstate
                write(ounit, frmt) real(t%cwf(i)), aimag(t%cwf(i))
            end do
            select case (ctrl%tdc_type) 
            case(1)
                do i = 1, t%nstate
                    write(ounit, frmt) t%olap1(i, 1:t%nstate)
                end do
            case(2) !< @todo Implement backup option for tdc_type 2
            end select
        end if
        close(ounit)

    end subroutine writebackup


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: ReadBackup
    !
    ! DESCRIPTION:
    !> @brief Read all variables required to contuinue a dynamics calculation to the backup file.
    !----------------------------------------------------------------------------------------------
    subroutine readbackup
        integer :: i
        integer :: iunit
        real(dp) :: cw(2)
        logical :: check

        inquire(file=ctrl%bufile, exist=check)
        if (.not. check) then
            write(stderr,*) 'Error. Backup file not found in directory while restarting.'
            write(stderr,*) '  Name of file: ', ctrl%bufile
            stop
        end if
        inquire(file=ctrl%outfile, exist=check)
        if (.not. check) then
            write(stderr,*) 'Warning. Output file not present in directory while restarting.'
            write(stderr,*) 'New output file will be created.'
            write(stderr,*)
            open(newunit=ctrl%ounit, file=ctrl%outfile)
        else
            open(newunit=ctrl%ounit, file=ctrl%outfile, action='READWRITE', position='APPEND')
        end if

         
        open(newunit=iunit, file=ctrl%bufile)
        read(iunit, *) t%step
        t%step = t%step + 1
        do i = 1, t%natom
            read(iunit, *) t%geom1(:, i)
        end do
        do i = 1, t%natom
            read(iunit, *) t%velo1(:, i)
        end do
        do i = 1, t%natom
            read(iunit, *) t%grad1(:, i)
        end do

        read(iunit, *) t%cstate
        read(iunit, *) t%qe1(1:t%nstate)
        if (ctrl%sh == 1) then
            read(iunit, *) t%qe0
        else if (ctrl%sh >= 2) then
            do i = 1, t%nstate
                read(iunit, *) cw(1), cw(2)
                t%cwf(i) = cmplx(cw(1), cw(2), kind = dp)
            end do
            select case (ctrl%tdc_type) 
            case(1)
                do i = 1, t%nstate
                    read(iunit, *) t%olap1(i, 1:t%nstate)
                end do
            case(2) !< @todo Implement restart option for tdc_type 2
            end select
        end if
        close(iunit)


    end subroutine readbackup


end module backupmod
