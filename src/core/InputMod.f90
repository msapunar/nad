!--------------------------------------------------------------------------------------------------
! MODULE: InputMod
!> @author Marin Sapunar, Ruđer Bošković Institute
!> @date December, 2016
!
! DESCRIPTION: 
!> @brief Input subroutines.
!> @details
!! Subroutines for setting default program options, reading command line arguments and reading 
!! program input files. 
!! Arrays of dimension t%natom are allocated in the ReadGeom subroutine. 
!! Arrays of dimension t%nstate are allocated in the ReadMainInp subroutine.
!--------------------------------------------------------------------------------------------------
module inputmod
    use fortutils
    use systemvar
    use controlvar
    use stringmod
    use constants

    implicit none

    private
    public :: readinput

    character(len=:), allocatable :: maininp !< Name of main input file.
    character(len=:), allocatable :: geominp !< Name of geometry input file.
    character(len=:), allocatable :: veloinp !< Name of velocity input file.
    character(len=:), allocatable :: pcinp !< Name of partial charges input file.
    real(dp) :: tol_cns

contains


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: DefaultOptions
    !
    ! DESCRIPTION:
    !> @brief Set program options to their default values.
    !> @details
    !! See manual for details concerning the input.
    !> @note Default values should be changeable without causing problems elsewhere in the program.
    !----------------------------------------------------------------------------------------------
    subroutine defaultoptions()
        ! File names.
        maininp = 'dynamics.in'
        geominp = 'geom'
        veloinp = 'veloc'
        pcinp = 'pcharge'
        ctrl%outfile = 'dynamics.out'
        ctrl%bufile = 'backup.dat'
        ctrl%buinterval = 1
        ! Method options.
        ctrl%qext = .true.
        ! Initial state.
        t%nstate = 1
        t%cstate = 1
        ! Print options.
        ctrl%print = .false.
        ctrl%print(1:6) = .true.
        ctrl%print(7:10) = .true.
        ctrl%print(11) = .true.
        ! Program flow options.
        ctrl%min01gap = 0.0_dp
        ! Nuclear dynamics options.
        ctrl%dt = 0.5_dp / aut_fs
        ctrl%nstep = 10
        ctrl%orientlvl = 0
        tol_cns = 0.000000000001_dp ! Default rattle tolerance.
        ! Surface hopping options.
        ctrl%sh = 2
        ctrl%shnstep = 10000
        ctrl%decohlvl = 1
        ctrl%couplvl = 0
        ctrl%coupndiff = 1
        ctrl%coupediff = 1 / eh_eV
        ctrl%tdc_type = 1
        ctrl%tdc_interpolate = 2
        ctrl%ene_interpolate = 2
        ctrl%vrescale = 1
        ctrl%fhop = 1
        ctrl%phaselvl = 1
        ctrl%oscill = .false.
        ! MM options.
        ctrl%mmcut = 10000.0_dp
        ctrl%pbc = .false.
        ! Point charges
        ctrl%pcharge = .false.
    end subroutine defaultoptions


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: ReadInput
    !
    ! DESCRIPTION:
    !> @brief Read all program input.
    !> @details 
    !! Read the command line arguments passed to the program, the main program input file and the 
    !! files containing the initial geometry and velocity of the system.
    !! At the moment, only the name of the main input file can be passed through the command line 
    !! arguments, all other options are set in this file.
    !----------------------------------------------------------------------------------------------
    subroutine readinput
        integer :: narg
        character(len=1000) :: temp
        integer :: i

        ! Get work directory.
        call getcwd(temp)
        ctrl%maindir = trim(adjustl(temp))//'/'

        ! Set default options.
        call defaultoptions()

        ! Get name of the main input file.
        narg = command_argument_count()
        if (narg > 0) then
            call get_command_argument(1, temp)
            maininp = temp
        end if

         ! Read main input file.
        write(stdout, *)
        write(stdout, '(1x,a,a,a)') 'Reading dynamics input file ', trim(maininp), '.'
        call readmaininp()
       
        ! Read initial conditions.
        write(stdout, *)
        write(stdout, '(1x,a,a,a)') 'Reading geometry file ', geominp, '.'
        call readgeom()
        write(stdout, '(1x,a,a,a)') 'Reading velocity file ', veloinp, '.'
        call readvelo()

        ! Read list of partial charges.
        if (ctrl%pcharge) then
            write(stdout, '(1x,a,a,a)') 'Reading partial charges file ', pcinp, '.'
            call readpc()
        end if

        ! Print initial information about the full system.
        write(stdout, *)
        write(stdout, '(1x,a,i0,a)') 'Starting calculation for a system with ', t%natom, ' atoms.'
        write(stdout, '(3x,a,i0,a)') 'QM system consists of ', t%qnatom, '.'
        write(stdout, '(3x,a,i0,a)') 'MM system consists of ', t%mnatom, '.'

        if (ctrl%qm) then
            write(stdout, *)
            write(stdout, '(3x,a)') 'Initial geometry of the QM system: '
            do i = 1, t%qnatom
                write(stdout, '(2x,a5,1000e18.10)') t%sym(t%qind(i)), t%geom2(:, t%qind(i))
            end do
            write(stdout, '(3x,a)') 'Initial velocity of the QM system: '
            do i = 1, t%qnatom
                write(stdout, '(2x,a5,1000e18.10)') t%sym(t%qind(i)), t%velo2(:, t%qind(i))
            end do
        end if
        if (ctrl%mm) then
            write(stdout, *)
            write(stdout, '(3x,a)') 'Initial geometry of the MM system: '
            do i = 1, t%mnatom
                write(stdout, '(2x,a5,1000e18.10)') t%sym(t%mind(i)), t%geom2(:, t%mind(i))
            end do
            write(stdout, '(3x,a)') 'Initial velocity of the MM system: '
            do i = 1, t%mnatom
                write(stdout, '(2x,a5,1000e18.10)') t%sym(t%mind(i)), t%velo2(:, t%mind(i))
            end do
        end if
    end subroutine readinput


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: ReadMainInp
    !
    ! DESCRIPTION:
    !> @brief Read the main program control variables.
    !> @details
    !! See manual for details concerning the input.
    !----------------------------------------------------------------------------------------------
    subroutine readmaininp()
        logical :: check
        logical :: buffer
        integer :: iunit
        integer, allocatable :: skipstates(:) !< States for which no couplings are calculated.
        integer :: i
        buffer = .false.

        inquire(file=maininp, exist=check)
        if (.not. check) then
            write(stderr,*) 'Error in Input module, ReadMainInp subroutine.'
            write(stderr,*) ' Main input file (', trim(maininp),') not found.'
            stop
        end if

        open(newunit=iunit, file=maininp, action='read')
        ! Mandatory keywords are read first:
        call readmethod(iunit)
        call readsystem(iunit, t%nstate, t%cstate, geominp, veloinp, t%ndim)

        ! Followed by optional keywords.
        call readdynamics(iunit)
        call readoutput(iunit)
        call readsurfhop(iunit, skipstates)
        call readmm(iunit)
        call readconstraints(iunit, tol_cns)
        call readrestart(iunit)
        close(iunit)

        ! Allocate all arrays of size t%nstate.
        allocate(t%qe1(t%nstate))
        allocate(t%qe2(t%nstate))
        if (t%nstate == 1) ctrl%sh = 0
        if (ctrl%print(11) .and. (t%nstate > 1)) then
            ctrl%oscill = .true.
            allocate(t%qo(t%nstate - 1))
        end if
        if (ctrl%sh == 1) then
            allocate(t%qe0(t%nstate))
            ctrl%print(7:10) = .false.
        else if (ctrl%sh >= 2) then
            allocate(ctrl%couple(t%nstate))
            allocate(t%cwf(t%nstate))
            allocate(t%prob(t%nstate))
            allocate(t%cmask(t%nstate, t%nstate))
            select case (ctrl%tdc_type)
            case(1)
                allocate(t%olap1(t%nstate, t%nstate))
                allocate(t%olap2(t%nstate, t%nstate))
                t%olap1 = 0.0_dp
                t%olap2 = 0.0_dp
            case(2)
                ! Nonadiabatic coupling vecotrs are allocated after reading number of atoms.
            end select
            ctrl%couple = .true.
            t%cwf = cmplx((0.0_dp, 0.0_dp), kind = dp)
            t%cwf(t%cstate) = cmplx((1.0_dp, 0.0_dp), kind = dp)
            t%prob = 0.0_dp
            if (allocated(skipstates)) then
                do i = 1, size(skipstates, 1)
                    if (skipstates(i) > t%nstate) exit
                    ctrl%couple(skipstates(i)) = .false.
                end do
            end if
        end if

    end subroutine readmaininp

 
    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: ReadGeom
    !
    ! DESCRIPTION:
    !> @brief Read the list of symbols, masses and positions of all atoms in the full system.
    !> @details 
    !! Read number of atoms in the full system and the QM and MM subsystems. Allocate all arrays
    !! of dimensions t%natom, t%qnatom or t%mnatom. Read the symbol, mass and position arrays for
    !! the full system.
    !! Assign QM and MM pointers to the appropriate sections of the main arrays.
    !----------------------------------------------------------------------------------------------
    subroutine readgeom()
        logical :: check
        integer :: iunit
        integer :: ios
        character(len=2) :: tsym
        real(dp) :: tmass
        character :: part
        integer :: i
        integer :: d

        inquire(file=geominp, exist=check)
        if (.not. check) then
            write(stderr,*) 'Error in Input module, ReadGeom subroutine.'
            write(stderr,*) ' Geometry file (', trim(geominp),') not found.'
            stop
        end if

        ! First run through the file to check number of atoms in the QM and MM part of the system.
        open(newunit=iunit, file=geominp, action='read')
        do
            call line%readline(iunit,comment='#',err=ios)
            if (is_iostat_end(ios)) exit
            call line%parse(' ,')
            if (line%narg < t%ndim + 3) then
                write(stderr, '(a)') 'Error in Input module, ReadGeom subroutine.'
                write(stderr, '(a,i0,a)') ' Need ', 3+t%ndim, ' arguments per line:'
                write(stderr, '(a)') '  symbol mass coords q/m'
                write(stderr, '(a,a)') '  Line: ', line%str
                stop 
            end if
            read(line%args(3+t%ndim), *) part
            select case (tolower(part))
            case('q')
                t%qnatom = t%qnatom + 1
            case('m')
                t%mnatom = t%mnatom + 1
            case default
                write(stderr, *) 'Error in Input module, ReadGeom subroutine.'
                write(stderr, *) ' Lines should end with "q" for QM atoms or "m" for MM atoms.'
                stop
            end select
        end do
        rewind(iunit)

        t%natom = t%qnatom + t%mnatom
        if (t%natom == 0) then
            write(stderr, *) 'Error in Input module, ReadGeom subroutine.'
            write(stderr, *) ' No atoms found in geometry file!'
            stop
        end if

        ! Allocate arrays of natom dimensions.
        allocate(t%sym(t%natom))
        allocate(t%mass(t%natom))
        allocate(t%chrg(t%natom))
        allocate(t%geom1(t%ndim, t%natom))
        allocate(t%velo1(t%ndim, t%natom))
        allocate(t%grad1(t%ndim, t%natom))
        allocate(t%geom2(t%ndim, t%natom))
        allocate(t%velo2(t%ndim, t%natom))
        allocate(t%grad2(t%ndim, t%natom))
        allocate(t%masi(t%natom, t%natom))
        if (t%qnatom > 0) then
            ctrl%qm = .true.
            allocate(t%qind(t%qnatom))
        else
            ctrl%qm = .false.
        end if
        if (t%mnatom > 0) then
            ctrl%mm = .true.
            allocate(t%mind(t%mnatom))
        else
            ctrl%mm = .false.
        end if
        if (ctrl%tdc_type == 2) then
            allocate(t%nadv1(t%ndim*t%natom, t%nstate, t%nstate))
            allocate(t%nadv2(t%ndim*t%natom, t%nstate, t%nstate))
        end if

        ! Second run through the file to read the atom, mass, position and index of each atom.
        t%qnatom = 0
        t%mnatom = 0
        t%masi = 0.0_dp
        do i = 1, t%natom
            call line%readline(iunit,comment='#',err=ios)
            call line%parse(' ,')

            ! Read symbol.
            read(line%args(1), *) tsym
            t%sym(i) = tolower(tsym)

            ! Read mass.
            read(line%args(2), *) tmass
            t%mass(i) = tmass * Da_me
            t%masi(i, i) = 1.0_dp / t%mass(i)

            ! Read initial geometry.
            do d = 1, t%ndim
                read(line%args(2+d), *) t%geom2(d, i)
            end do

            ! Check part (q = qm, m = mm)
            read(line%args(3+t%ndim), *) part
            select case (tolower(part))
            case('q')
                t%qnatom = t%qnatom + 1
                t%qind(t%qnatom) = i
            case('m')
                t%mnatom = t%mnatom + 1
                t%mind(t%mnatom) = i
            end select
        end do
        close(iunit)
    end subroutine readgeom

 
    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: ReadVelo
    !
    ! DESCRIPTION:
    !> @brief Read the velocities of the atoms in the system.
    !----------------------------------------------------------------------------------------------
    subroutine readvelo()
        logical :: check
        integer :: iunit
        integer :: ios
        integer :: i
        integer :: d

        inquire(file=veloinp, exist=check)
        if (.not. check) then
            write(stderr, *) 'Error in Input module, ReadVelo subroutine.'
            write(stderr, *) ' Velocity file (', trim(veloinp),') not found.'
            stop
        end if

        open(newunit=iunit, file=veloinp, action='read')
        do i = 1, t%natom
            call line%readline(iunit,comment='#',err=ios)
            if (ios < 0) then
                write(stderr, *) 'Error in Input module, ReadVelo subroutine.'
                write(stderr, '(a)') ' Unexpected end of file.'
                stop
            end if
            call line%parse(' ,')
            if (line%narg < t%ndim) then
                write(stderr, *) 'Error in Input module, ReadVelo subroutine.'
                write(stderr, '(a,i0,a)') ' Premature end of line, expect ', t%ndim, ' arguments.'
                write(stderr, '(a,a)') ' Line: ', line%str
                stop
            end if
            do d = 1, t%ndim
                read(line%args(d), *) t%velo2(d, i)
            end do
        end do
        close(iunit)
    end subroutine readvelo


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: ReadPC
    !
    ! DESCRIPTION:
    !> @brief Read the partial charges of the atoms in the system.
    !----------------------------------------------------------------------------------------------
    subroutine readpc()
        logical :: check
        integer :: iunit
        integer :: ios
        integer :: i

        inquire(file=pcinp, exist=check)
        if (.not. check) then
            write(stderr, *) 'Error in Input module, ReadPC subroutine.'
            write(stderr, *) ' Point charges file (', trim(pcinp),') not found.'
            stop
        end if

        open(newunit=iunit, file=pcinp, action='read')
        do i = 1, t%natom
            call line%readline(iunit,comment='#',err=ios)
            if (ios < 0) then
                write(stderr, *) 'Error in Input module, ReadPC subroutine.'
                write(stderr, '(a,i0,a)') ' Non-zero return code (', ios, ') from read statement.'
                stop
            end if
            read(line%str, *) t%chrg(i)
        end do
        close(iunit)
    end subroutine readpc
 

    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: ReadMethod
    !
    ! DESCRIPTION:
    !> @brief Read the $method section of the main input file.
    !> @details 
    !! In this section, programs to run the QM and MM calculations are selected.
    !----------------------------------------------------------------------------------------------
    subroutine readmethod(iunit)
        integer, intent(in) :: iunit
        integer :: ios

        if (.not. allocated(ctrl%qprog)) ctrl%qprog = ''
        if (.not. allocated(ctrl%mprog)) ctrl%mprog = ''

        write(stdout, '(3x,a)') 'Reading $method section of the dynamics input file.'
        rewind(iunit)
 outer: do
            call line%readline(iunit,comment='#',err=ios)
            if (ios < 0) then
                write(stderr,'(a)') 'Error in InputSections module, ReadMethod subroutine.'
                write(stderr,'(2x,a)') 'Method section not found.'
                stop
            end if
            if (index(line%str, '$method') /= 1) cycle
 inner:     do 
                call line%readline(iunit,comment='#')
                if (index(line%str, '$') == 1) exit outer
                call line%parse(' ')
                select case(trim(adjustl(line%args(1))))
                case('qm')
                    ctrl%qprog = trim(adjustl(line%args(2)))
                    write(stdout, '(5x,a)') '"'//ctrl%qprog//'" will be used for QM calculations.'
                case('mm')
                    ctrl%mprog = trim(adjustl(line%args(2)))
                    write(stdout, '(5x,a)') '"'//ctrl%qprog//'" will be used for MM calculations.'
                case('model')
                    ctrl%qext = .false.
                end select
            end do inner
        end do outer
    end subroutine readmethod


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: ReadSystem
    !
    ! DESCRIPTION:
    !> @brief Read information about the system.
    !> @details
    !! See manual for details concerning the input.
    !----------------------------------------------------------------------------------------------
    subroutine readsystem(iunit, nstate, cstate, geominp, veloinp, ndim)
        integer, intent(in) :: iunit
        integer, intent(inout) :: nstate
        integer, intent(out) :: cstate
        character(len=:), allocatable :: geominp
        character(len=:), allocatable :: veloinp
        integer, intent(out) :: ndim
        integer :: ios

        rewind(iunit)
 outer: do
            call line%readline(iunit,comment='#',err=ios)
            if (ios < 0) then
                write(stderr,'(a)') 'Error in InputSections module, ReadSystem subroutine.'
                write(stderr,'(2x,a)') 'System section not found.'
                stop
            end if
            if (index(line%str, '$system') /= 1) cycle
 inner:     do
                call line%readline(iunit,comment='#',err=ios)
                if (ios < 0) exit outer
                if (index(line%str, '$') == 1) exit outer
                call line%parse(' =')
                select case(line%args(1))
                case('nstate')
                    read(line%args(2), *) nstate
                case('istate')
                    read(line%args(2), *) cstate
                case('geometry')
                    geominp = trim(adjustl(line%args(2)))
                case('velocity')
                    veloinp = trim(adjustl(line%args(2)))
                case('ndim')
                    read(line%args(2), *) ndim
                case default
                    write(stderr, *) 'Warning in InputSection module, ReadSystem subroutine.' 
                    write(stderr, *) '  Skipping line with unrecognized keyword:' 
                    write(stderr, *) line%str
                end select
            end do inner
        end do outer

        if (nstate == 0) then
            write(stderr,*) 'Error in InputSection module, ReadSystem subroutine.'
            write(stderr,'(a)') ' Number of states not defined.'
            stop
        end if
        if (cstate == 0) then
            write(stderr,*) 'Warning in InputSection module, ReadSystem subroutine.'
            write(stderr,'(a)') ' Initial state not defined, setting initial state = ', nstate, '.'
            cstate = nstate
        end if

    end subroutine readsystem


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: ReadOutput
    !
    ! DESCRIPTION:
    !> @brief Read output options of the program.
    !> @details
    !! See manual for details concerning the input.
    !----------------------------------------------------------------------------------------------
    subroutine readoutput(iunit)
        integer, intent(in) :: iunit
        integer, allocatable :: printopts(:)
        integer :: ios
        integer :: i

        rewind(iunit)
 outer: do
            call line%readline(iunit,comment='#',err=ios)
            if (ios < 0) exit outer
            if (index(line%str, '$output') /= 1) cycle
 inner:     do
                call line%readline(iunit,comment='#',err=ios)
                if (ios < 0) exit outer
                if (index(line%str, '$') == 1) exit outer
                call line%parse(' =')
                select case(line%args(1))
                case('outfile')
                    ctrl%outfile = trim(line%args(2))
                case('print')
                    call readindexlist(line%str(6:), printopts)
                    do i = 1, size(printopts)
                        ctrl%print(printopts(i)) = .true.
                    end do
                case('noprint')
                    call readindexlist(line%str(8:), printopts)
                    do i = 1, size(printopts)
                        ctrl%print(printopts(i)) = .false.
                    end do
                case('bufile')
                    ctrl%bufile = trim(line%args(2))
                case('buinterval')
                    read(line%args(2), *) ctrl%buinterval
                case default
                    write(stderr, *) 'Warning in InputSection module, ReadOutput subroutine.' 
                    write(stderr, *) '  Skipping line with unrecognized keyword:' 
                    write(stderr, *) line%str
                end select
            end do inner
        end do outer
    end subroutine readoutput


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: ReadDynamics
    !
    ! DESCRIPTION:
    !> @brief Read information about the nuclear dynamics.
    !> @details
    !! See manual for details concerning the input.
    !----------------------------------------------------------------------------------------------
    subroutine readdynamics(iunit)
        integer, intent(in) :: iunit
        integer :: ios

        rewind(iunit)
 outer: do
            call line%readline(iunit,comment='#',err=ios)
            if (ios < 0) exit outer
            if (index(line%str, '$dynamics') /= 1) cycle
 inner:     do
                call line%readline(iunit,comment='#',err=ios)
                if (ios < 0) exit outer
                if (index(line%str, '$') == 1) exit outer
                call line%parse(' =')
                select case(line%args(1))
                case('nstep')
                    read(line%args(2), *) ctrl%nstep
                case('tstep')
                    read(line%args(2), *) ctrl%dt
                    ctrl%dt = ctrl%dt / aut_fs
                case('orient')
                    read(line%args(2), *) ctrl%orientlvl
                    !> @todo Implement orient.
                    stop 'Orient option not implemented.'
                case default
                    write(stderr, *) 'Warning in InputSection module, ReadDynamics subroutine.' 
                    write(stderr, *) '  Skipping line with unrecognized keyword:' 
                    write(stderr, *) line%str
                end select
            end do inner
        end do outer
    end subroutine readdynamics


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: ReadConstraints
    !
    ! DESCRIPTION:
    !> @brief Read information about constraints.
    !> @details
    !! See manual for details concerning the input.
    !----------------------------------------------------------------------------------------------
    subroutine readconstraints(iunit, default_tol)
        integer, intent(in) :: iunit
        real(dp), intent(in) :: default_tol
        integer :: ios
        integer :: i

        ctrl%n_cns = 0
        rewind(iunit)
 first: do
            call line%readline(iunit,comment='#',err=ios)
            if (ios < 0) exit first
            if (index(line%str, '$constraints') /= 1) cycle
            do
                call line%readline(iunit,comment='#',err=ios)
                if (ios < 0) exit first
                if (index(line%str, '$') == 1) exit first
                ctrl%n_cns = ctrl%n_cns + 1
            end do
        end do first

        if (ctrl%n_cns == 0) return
        ctrl%constraint = .true.
        allocate(ctrl%cns(ctrl%n_cns))
        ctrl%cns(:)%tol = default_tol
        rewind(iunit)
 secnd: do
            call line%readline(iunit,comment='#',err=ios)
            if (index(line%str, '$constraints') /= 1) cycle
            do i = 1, ctrl%n_cns
                call line%readline(iunit,comment='#',err=ios)
                call line%parse(' =,')
                select case(line%args(1))
                case('b')
                    read(line%args(2), *) ctrl%cns(i)%i(1)
                    read(line%args(3), *) ctrl%cns(i)%i(2)
                    read(line%args(4), *) ctrl%cns(i)%cval
                    if (line%narg > 4) read(line%args(5), *) ctrl%cns(i)%tol
                case default
                    write(stderr, *) 'Error in InputSection module, ReadConstraints subroutine.' 
                    write(stderr, *) '  Unrecognized constraint type: ', trim(adjustl(line%args(1)))
                    stop
                end select
            end do
            exit secnd
        end do secnd
    end subroutine readconstraints


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: ReadSurfHop
    !
    ! DESCRIPTION:
    !> @brief Read the $surfhop section of the main input file.
    !> @details
    !! See manual for details concerning the input.
    !----------------------------------------------------------------------------------------------
    subroutine readsurfhop(iunit, skipstates)
        integer, intent(in) :: iunit
        integer, allocatable, intent(out) :: skipstates(:)
        integer :: ios

        rewind(iunit)
 outer: do
            call line%readline(iunit,comment='#',err=ios)
            if (ios < 0) exit outer
            if (index(line%str, '$surfhop') /= 1) cycle
            call line%parse(' =')
            if (line%narg > 1) then
                select case (trim(line%args(2)))
                case('off')
                    ctrl%sh = 0
                    return
                end select
            end if
 inner:     do
                call line%readline(iunit,comment='#',err=ios)
                if (ios < 0) exit outer
                if (index(line%str, '$') == 1) exit outer
                call line%parse(' =')
                select case(line%args(1))
                case('lz')
                    ctrl%sh = 1
                case('fssh')
                    ctrl%sh = 2
                    if (line%narg > 1) then
                        select case (trim(line%args(2)))
                        case('adiabatic')
                            ctrl%sh = 2
                        case('diabatic')
                            ctrl%sh = 3
                        end select
                    end if
                case('tdse_steps')
                    read(line%args(2), *) ctrl%shnstep
                case('decoherence')
                    select case(line%args(2))
                    case('off')
                        ctrl%decohlvl = 0
                    case('nldm')
                        ctrl%decohlvl = 1
                    case default
                        write(stderr, *) 'Error in InputSection module, ReadSurfHop subroutine.'
                        write(stderr, '(2x,a)') '  Unrecognized decoherence method: '
                        write(stderr, '(2x,a)') line%str
                        stop
                    end select
                case('overlap')
                    ctrl%tdc_type = 1
                    if (line%narg > 1) then
                        select case(line%args(2))
                        case('constant')
                            ctrl%tdc_interpolate = 1
                        case('linear')
                            ctrl%tdc_interpolate = 2
                        case('npi')
                            ctrl%tdc_interpolate = 4
                        end select
                    end if
                case('nadvec')
                    ctrl%tdc_type = 2
                    ctrl%print(9) = .false. !< @todo Move this.
                case('energy')
                    select case(line%args(2))
                    case('constant')
                        ctrl%ene_interpolate = 0
                    case('step')
                        ctrl%ene_interpolate = 1
                    case('linear')
                        ctrl%ene_interpolate = 2
                    end select
                case('velocity')
                    select case(line%args(2))
                    case('off')
                        ctrl%vrescale = 0
                    case('vel')
                        ctrl%vrescale = 1
                    case('gdif')
                        ctrl%vrescale = 2
                    case('nadvec')
                        ctrl%vrescale = 3
                    end select
                case('frustrated')
                    select case(line%args(2))
                    case('continue')
                        ctrl%fhop = 1
                    case('reverse')
                        ctrl%fhop = 2
                    end select
                case('phase')
                    select case(line%args(2))
                    case('off')
                        ctrl%phaselvl = 0
                    case('diagonal')
                        ctrl%phaselvl = 1
                    case('assigned')
                        ctrl%phaselvl = 2
                    case default
                        write(stderr, *) 'Error in InputSection module, ReadSurfHop subroutine.'
                        write(stderr, '(2x,a)') '  Unrecognized phase matching algorithm: '
                        write(stderr, '(2x,a)') line%str
                        stop
                    end select
                case('mingap')
                    read(line%args(2), *) ctrl%min01gap
                case('skip_state')
                    call readindexlist(line%str(11:), skipstates)
                case('seed')
                    read(line%args(2), *) ctrl%seed
                case default
                    write(stderr, *) 'Warning in InputSection module, ReadSurfHop subroutine.'
                    write(stderr, *) '  Skipping line with unrecognized keyword:' 
                    write(stderr, *) line%str
                end select
            end do inner
        end do outer
    end subroutine readsurfhop

    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: ReadMM
    !
    ! DESCRIPTION:
    !> @brief Read the $mm section of the main input file.
    !> @details
    !! See manual for details concerning the input.
    !----------------------------------------------------------------------------------------------
    subroutine readmm(iunit)
        integer, intent(in) :: iunit
        integer :: ios

        rewind(iunit)
 outer: do
            call line%readline(iunit, comment='#', err=ios)
            if (ios < 0) exit outer
            if (index(line%str, '$mm') /= 1) cycle
 inner:     do
                call line%readline(iunit,comment='#',err=ios)
                if (ios < 0) exit outer
                if (index(line%str, '$') == 1) exit outer
                call line%parse(' =')
                select case(line%args(1))
                case('cutoff')
                    read(line%args(2), *) ctrl%mmcut
                case('pcharge')
                    ctrl%pcharge = .true.
                    if (any(line%args == 'off')) ctrl%pcharge = .false.
                case('pcharge_file')
                    pcinp = trim(adjustl(line%args(2)))
                case('pbc')
                    ctrl%pbc = .true.
                    if (any(line%args == 'off')) then
                        ctrl%pbc = .false.
                    else
                        call line%readline(iunit, comment ='#')
                        read (line%str, *) t%pbcbox
                    end if
                case default
                    write(stderr, *) 'Warning in InputSection module, ReadMM subroutine.' 
                    write(stderr, *) '  Skipping line with unrecognized keyword:' 
                    write(stderr, *) line%str
                end select
            end do inner
        end do outer
    end subroutine readmm


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: ReadRestart
    !
    ! DESCRIPTION:
    !> @brief Check if the $restart keyword is present in the main input file.
    !> @details
    !! See manual for details concerning the input.
    !----------------------------------------------------------------------------------------------
    subroutine readrestart(iunit)
        integer, intent(in) :: iunit
        integer :: ios

        rewind(iunit)
 outer: do
            call line%readline(iunit,comment='#',err=ios)
            if (ios < 0) exit outer
            if (index(line%str, '$restart') == 1) then
                ctrl%restart = .true.
                return
            end if
        end do outer
    end subroutine readrestart

 
end module inputmod
